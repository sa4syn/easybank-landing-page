document.getElementById("menu").addEventListener("click", myFunction);
function myFunction() {
  var x = document.getElementById("tool-bar");
  if (x.style.display === "none") {
    x.style.display = "block";
    document.getElementById("menu").style.backgroundImage = 'url("images/icon-close.svg")';
  } else {
    x.style.display = "none";
    document.getElementById("menu").style.backgroundImage = 'url("images/icon-hamburger.svg")';
  }
}

var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
  var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("nav-bar").style.top = "0px";
  } else {
    document.getElementById("nav-bar").style.top = "-4rem";
  }
  prevScrollpos = currentScrollPos;
}